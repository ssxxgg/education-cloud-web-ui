 <p align="center">
   <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="Build Status">
  <img src="https://img.shields.io/badge/license-Apache%202-blue.svg" alt="Build Status">
   <img src="https://img.shields.io/badge/Spring%20Cloud-Hoxton.SR2-blue.svg" alt="Coverage Status">
   <img src="https://img.shields.io/badge/SpringCloudAlibaba-2.2.0.RELEASE-blue.svg?style=flat-square" alt="Coverage Status">
   <img src="https://img.shields.io/badge/Spring%20Boot-2.2.5.RELEASE-blue.svg" alt="Downloads">
 </p>  
<div align=center><h3>分布式在线教育</h3></div>
<h4 align=center>一个各行业都适用的分布式在线教育系统。该系统基于Spring Cloud Alibaba微服务化开发平台， 核心技术采用Spring Boot 2.2.0以及Spring Cloud (Hoxton.RELEASE) 相关核心组件，采用Nacos注册和配置中心，前端采用vue-element-admin组件</h4>

### 相关工程
##### 在线教育系统(education-cloud)：[码云地址](https://gitee.com/wewwww/education-cloud.git) 
##### 前端门户工程(education-cloud-web-ui)：[码云地址](https://gitee.com/wewwww/education-cloud-web-ui.git)
##### 后台管理工程(education-cloud-web-admin)：[码云地址](https://gitee.com/wewwww/education-cloud-web-admin.git)
> 在线教育系统采用前后端分离架构，前端为独立工程。  
- education-cloud是后台工程，核心框架：Spring Cloud Alibaba 
- education-cloud-web是前端门户工程，核心框架：Vuejs + Nuxt.js  
- education-cloud-admin是后台管理工程，核心框架：vue-element-admin

### 前台主要功能介绍
* 首页功能，导航模块（自定义导航设置），广告模块（自定的轮播设置），课程模块（自定义课程设置）
* 列表功能，分类模块（自定义分类设置），搜索模块（自定义搜索设置）
* 课程详情页功能，课程介绍、目录的展示和购买、播放功能等等
* 个人中心，具有个人信息设置、密码修改、订单管理、学习记录等功能
* 讲师中心，讲师信息管理、课程管理（课程的添加、修改）、收益管理等功能

### 后台主要功能介绍
* 权限管理功能，多角色多用户自定义配置
* 系统配置功能，自定义进行站点配置及第三方参数配置
* 讲师管理功能，讲师申请入驻，后台具有审核功能
* 课程管理功能，讲师管理自有课程，后台具有审核功能
* 用户登录功能，同一时间只允许同一个账号在同一个地方登录，防止账号共享
* 广告管理功能，后台自定义广告设置，增加营销效果
* 支付功能模块，待集成微信和支付宝支付

<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=eda8d54aba0a0d56d9030be06b7b8fdd07f438bf113b7f11fd8541fc9f181feb"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="分布式在线教育" title="分布式在线教育">1093045884 可加</a>
> QQ群：1093045884

#### 编译使用
``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:5800
$ npm run dev

# build for production and launch server
$ npm run build

```
#### 基于Docker容器打包部署
 
- 确保本机已安装docker以及docker compose环境
- git https://gitee.com/wewwww/education-cloud-web-ui.git
- 部署打包之前确认配置文件后端API网关调用接口地址：详见config/conf.js文件
- 进入到根目录执行命令 `docker build -t 127.0.0.1/edu-cloud/education-cloud-ui-web:latest .` 即可构建镜像 说明127.0.0.1/edu-cloud是Harbor仓库镜像前缀，根据自己的仓库调整
- 前端项目需要手动推送到远程仓库,比如Harbor仓库 命令： docker push 127.0.0.1/edu-cloud/education-ui-web:latest

